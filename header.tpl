{*
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

*}
{include file="_head.tpl" theme_template=true}
<body onMouseOut="closeMenus();">
<a name="top"></a>
{$DCSCRIPT}
<div id="header">
    {include file="_companyLogo.tpl" theme_template=true}
    {include file="_globalLinks.tpl" theme_template=true}
    {include file="_welcome.tpl" theme_template=true}
    <div id="cic_toolbar_stats" style="position: relative; float:left; margin-top: 20px; margin-left: 20px; visibility:hidden;">
      <span id="cic_toolbar_workgroupforstats" style="position: absolute;top: 0;margin-top: -16px;width: 100px;">Customer Service</span>
      <div id="cic_toolbar_interactioncount" style="float:left; position:absolute;"></div>
      <strong id="cic_toolbar_interactionnumber" style="float: left; position: absolute; text-align: center; width: 48px;"></strong>
      <div id="cic_toolbar_workgroupstats" style="float:left; position: absolute; left: 54px; top:-1px;"></div>
      <strong style="float: left; position: absolute; text-align: center; width: 32px;left: 85px;font-size: 2em;"><strong id="cic_toolbar_serviceLevel">100</strong><strong style="font-size:0.5em;">%</strong></strong>
    </div>
    <div class="clear"></div>
    {include file="_headerSearch.tpl" theme_template=true}
    <div class="clear"></div>
    {if !$AUTHENTICATED}
    <br /><br />
    {/if}
    <div id="ajaxHeader">
        {include file="_headerModuleList.tpl" theme_template=true}
    </div>
    <div class="clear"></div>
    <div class="line"></div>
    <div>
      <div id="cic_chat_windows" style="position: fixed; bottom: 0; float: right; z-index: 1000; right: 0;">
      </div>
      <div id="cic_agent_toolbar" style="margin: 2px;"></div>

      <div>
        <script type="text/template" id="cic-template-chatwindow">
        <div id="cic_chat_window_<%=interactionId%>" style="position:relative; bottom:0; float: right; display: block; margin-right: 8px; width: 200px; height: 320px; background-image: linear-gradient( to top right, #FFF, #F0F0F0 ); z-index: 1000; box-shadow: -1px 2px 5px 1px rgba(0, 0, 0, 0.7); border-radius: 0.2em 0.25em 0 0; visibility:hidden;">
          <div style="position:relative; box-shadow: rgba(0, 0, 0, 0.3) -1px 2px 5px 0px; border-radius: 0.2em 0.25em 0px 0px; margin: 0 0 5px 0; padding: 2px;">
            <img id="cic_chatwindow_govideo" src="cic/img/Video.png" style="position:absolute; float: left; bottom:0; left:0; z-index:1000; margin-left: 2px;margin-right: 2px;margin-top: -1px; height:14px;cursor: pointer;" >
            <em id="cic_chatwindow_title" style="margin-left:16px;"><%=title%></em>
            <img id="cic_chatwindow_minimize" src="cic/img/bullet_arrow_down.png" style="float: right;margin-right: 6px;margin-top: 2px;cursor: pointer;">
          </div>
          <ul id="cic_chatwindow_messages" style="margin: 0; padding: 10px 0 0 0; list-style-type: none; overflow: auto; height: 240px;"></ul>
          <div style="width: 200px; padding-left: 2px; padding-right: 2px; position: absolute; bottom: 2px;">
            <textarea id="cic_chatwindow_messagetosend" style="display: inline-block; float: left; clear: both; width: 148px; height: 38px; "></textarea>
            <button id="cic_chatwindow_send" style=" display: inline-block; float: left; width: 42px; height: 44px;">Send</button>
          </div>
        </div>
        </script>

        <script type="text/template" id="cic-template-logonform">
  			<form action="javascript:void(0);">
          <div style="float:left; position: relative; margin-bottom:2px;">
  			       <input type="text" id="cic_logonform_username" placeholder="Username" style="height:24px; padding-left:30px;">
               <img src="cic/img/user.png" style="height:24px; position: absolute; left: 2px; top:2px;">
          </div>
          <div style="float:left; position: relative; margin-left:4px;">
              <input type="password" id="cic_logonform_password" placeholder="Password" style="height:24px; padding-left:30px;">
              <img src="cic/img/password.png" style="height:24px; position: absolute; left: 2px; top:2px;">
          </div>
          <div style="float:left; position: relative; margin-left:4px;">
              <input type="text" id="cic_logonform_cicserver" placeholder="IC Server" style="height:24px; padding-left:30px;">
              <img src="cic/img/inin.gif" style="height:24px; position: absolute; left: 2px; top:2px;">
          </div>
          <div style="float:left; position: relative;">
              <select id="cic_stationform_type" style="height:28px; margin-left:4px; padding-left:28px; float:left; position:relative;">
                <option value="workstation" selected>Workstation</option>
                <option value="remote_workstation">Remote Workstation</option>
                <option value="remote_number">Remote Number</option>
              </select>
  			      <input type="text" id="cic_stationform_name" placeholder="Station Name" style="height:24px; width: 80px; padding-left:8px;">
              <img src="cic/img/telephone.png" style="height:24px; position: absolute; left: 6px; top:2px;">
          </div>
  			  <button id="cic_logonform_logon" style="height:28px; margin-left:4px;">Logon</button>
  			</form>
  		  </script>

        <script type="text/template" id="cic-template-activetoolbar">
  			<form action="javascript:void(0);">
          <div style="position: relative;">
            <img src="cic/img/inin.gif" style="float: left;padding: 0;margin: 1px 4px 1px 1px;display: block; width:28px;">
            <div id="cic_toolbar_interactioncount" style="float:left;"></div>
            <strong id="cic_toolbar_interactionnumber" style="float: left; position: absolute; left: 36px; text-align: center; width: 32px;"></strong>
            <div id="cic_toolbar_workgroupstats" style="float:left; margin-top:16px; position: absolute; left: 36px; top:-1px;"></div>
          </div>
          <div id="cic_toolbar_actionbar" style="position: relative;"></div>
          <div id="cic_toolbar_statusbar"></div>
          <button id="cic_toolbar_Logoff" style="float: left;padding: 0;margin: 0px 0px 1px 4px;display: block;">
            <img src="cic/img/logoff.svg" style="width:28px;">
          </button>
          <div id="cic_toolbar_interactions" style="display: inline-block; height: 30px; padding-left: 8px;"></div>
        </form>
  		  </script>

        <script type="text/template" id="cic-template-actionbar">
          <button id="cic_toolbar_Pickup" style="float: left;padding: 0;margin: 0px 0px 1px 1px;display: block;" <%=(capabilities.Pickup==0)?'disabled':''%>>
            <img src="cic/img/pickup.svg" style="width:28px;<%=(capabilities.Pickup==0)?'opacity:0.2;cursor:not-allowed;':''%>">
          </button>
          <button id="cic_toolbar_Disconnect" style="float: left;padding: 0;margin: 0px 0px 1px 0px;display: block;" <%=(capabilities.Disconnect==0)?'disabled':''%>>
            <img src="cic/img/disconnect.svg" style="width:28px;<%=(capabilities.Disconnect==0)?'opacity:0.2;cursor:not-allowed;':''%>">
          </button>
          <button id="cic_toolbar_Hold" style="position: relative; float: left;padding: 0;margin: 0px 0px 1px 0px;display: block;" <%=(capabilities.Hold==0)?'disabled':''%>>
            <img src="cic/img/hold.svg" style="width:28px;<%=(capabilities.Hold==0)?'opacity:0.2;cursor:not-allowed;':''%>">
            <div style="height: 6px; background-color: #5bc0de; position: absolute; width: 28px; bottom: 0px; display: block; float: left; opacity: 0.8; <%=(interaction == null || interaction && !interaction.isOnHold())?'visibility:hidden;':''%>"></div>
          </button>
          <button id="cic_toolbar_Mute" style="position: relative; float: left;padding: 0;margin: 0px 0px 1px 0px;display: block;" <%=(capabilities.Mute==0)?'disabled':''%>>
            <img src="cic/img/mute.svg" style="width:28px;<%=(capabilities.Mute==0)?'opacity:0.2;cursor:not-allowed;':''%>">
            <div style="height: 6px; background-color: #5bc0de; position: absolute; width: 28px; bottom: 0px; display: block; float: left; opacity: 0.8; <%=(interaction == null || interaction && !interaction.isMuted())?'visibility:hidden;':''%>"></div>
          </button>
          <input type="text" placeholder="Phone Number" id="cic_toolbar_Transfer_target" style="position: relative; float: left;padding: 0px 0px 0px 8px; margin: 0px 0px 1px 0px; display: block; height:28px;"/>
          <button id="cic_toolbar_Transfer" style="position: relative; float: left;padding: 0;margin: 0px 0px 1px 0px;display: block;" <%=(capabilities.Transfer==0)?'disabled':''%>>
            <img src="cic/img/transfer.svg" style="width:28px;<%=(capabilities.Transfer==0)?'opacity:0.2;cursor:not-allowed;':''%>">
          </button>
          <button id="cic_toolbar_Record" style="position: relative; float: left; padding: 0; margin: 0px 4px 1px 0px; display: block; <%=(capabilities.Record==0)?'disabled':''%>">
            <img src="cic/img/record.svg" style="width:28px;<%=(capabilities.Record==0)?'opacity:0.2;cursor:not-allowed;':''%>">
            <div style="height: 6px; background-color: #d43f3a; position: absolute; width: 28px; bottom: 0px; display: block; float: left; opacity: 0.8; <%=(interaction == null || interaction && !interaction.isRecording())?'visibility:hidden;':''%>"></div>
          </button>
        </script>

        <script type="text/template" id="cic-template-selectstatus">
        <select id="cic_toolbar_selectstatus" style="float:left;display:block;"></select>
  		  </script>

        <script type="text/template" id="cic-template-interaction">
        <span id="cic_toolbar_interaction_<%=interaction.interactionId%>" style="background-color: <%=color%>; color: #FFFFFF; padding: .2em .6em .3em; text-align: center; white-space: nowrap; border-radius: .25em; margin: 2px 0 3px 2px; display: inline-block; <%=selected?'box-shadow: -1px 2px 5px 1px rgba(0, 0, 0, 0.7); top: -1px; position: relative;':'opacity: 0.9;'%>">
          <img src="cic/img/<%=interaction.attributes.Eic_ObjectType%>.png" style="width: 16px; height: 16px; cursor: pointer; margin-top: 2px; display: block; float: left;">
          <%= interaction.isRecording()?'<img src="cic/img/recording.png" style="width: 16px; height: 16px; cursor: pointer;">':''%>
          <span style="margin: 0 6px 4px 4px; position: relative; top: -1px; cursor: pointer; display: block; float: left;"><%=interaction.attributes.Eic_AssignedWorkgroup%></span>
          <span style="margin-bottom: 4px; position: absolute; top: 13px; left:28px; cursor: pointer; display: block; float: left; font-size: 0.8em">#<%=interaction.interactionId%></span>
          <img id="cic_toolbar_interaction_goto_<%=interaction.interactionId%>" src="cic/img/folder_go.png" style="width: 16px; height: 16px; position: relative; top: 3px; margin-left: 4px; cursor: pointer; display:block;">
        </span>
        </script>

        <script type="text/template" id="cic-template-chatline-customer">
        <li style="clear: both; float: left; margin: 0 0 24px 6px; position:relative;">
          <strong style="position: absolute; left: 0; width: 100px; font-size: 0.9em; margin-left: 6px;"><%=participant%></strong>
          <em style="position: absolute; left: 0; bottom: -12px; width: 120px; font-size: 0.9em; margin-left: 6px; color: #999999;"><%=lastUpdate%></em>
          <p style="margin: 12px 0 0 4px; text-align: left; float: left; display: inline-block; padding: 4px 6px; max-width: 150px; background-color: #FFF; border-radius: 3px; box-shadow: 0px 3px 3px #E3E4E6; color: #637277; line-height: 1.4; word-wrap: break-word;"><%=message%></p>
        </li>
        </script>

        <script type="text/template" id="cic-template-chatline-agent">
          <li style=" clear: both; float: right; margin: 0 6px 24px 0; position:relative">
            <p style="margin: 12px 4px 4px 0; text-align: left; float: left; display: inline-block; padding: 4px 6px; max-width: 150px; border-radius: 3px; box-shadow: 0px 3px 3px rgba(255, 102, 0, 0.8); color: #FFF; line-height: 1.4; word-wrap: break-word; background-color:#F60;">
              <%=message%></p>
            <strong style="position: absolute; right: 0; width: 100px; font-size: 0.9em; text-align: right; margin-right: 12px;"><%=participant%></strong>
            <em style="position: absolute; right: 0; bottom: -12px; width: 120px; font-size: 0.9em; text-align: right; margin-right: 12px; color: #999999;"><%=lastUpdate%></em>
          </li>
        </script>

  		</div>
      <script src="cic/jquery.ddslick.js"></script>
      <script src="cic/jquery.sparkline.min.js"></script>
      <script src="cic/icws.js"></script>
      <script src="cic/underscore-min.js"></script>
    	<script src="cic/agent_toolbar.js"></script>
    <div class="clear"></div>
    <div class="line"></div>
</div>

{literal}
<iframe id='ajaxUI-history-iframe' src='index.php?entryPoint=getImage&imageName=blank.png'  title='empty' style='display:none'></iframe>
<input id='ajaxUI-history-field' type='hidden'>
<script type='text/javascript'>
if (SUGAR.ajaxUI && !SUGAR.ajaxUI.hist_loaded)
{
    YAHOO.util.History.register('ajaxUILoc', "", SUGAR.ajaxUI.go);
    {/literal}{if $smarty.request.module != "ModuleBuilder"}{* Module builder will init YUI history on its own *}
    YAHOO.util.History.initialize("ajaxUI-history-field", "ajaxUI-history-iframe");
    {/if}{literal}
}
</script>
{/literal}

<div id="main">
    <div id="content" {if !$AUTHENTICATED}class="noLeftColumn" {/if}>
        <table style="width:100%"><tr><td>
