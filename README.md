README
======

This repository contains a toolbar that integrates with CIC via ICWS

![Screenshot](https://bytebucket.org/laurentmillan/sugarcrm-toolbar/raw/14041d9cc2fbae10602b0e88252c9407cf321276/screenshot.png)

### What is this repository for?

-	Showing how easy it is to integrate with ICWS

### How do I get set up?

-	Install SugarCRM or SuiteCRM
-	git clone 'https://bitbucket.org/laurentmillan/sugarcrm-toolbar.git
-	Backup the following file: C:\inetpub\wwwroot\<Your CRM Folder>\themes\Sugar5\tpls\header.tpl
-	Copy the following files and folders:
	-	header.tpl: Copy to C:\inetpub\wwwroot\<Your CRM Folder>\themes\Sugar5\tpls\\
	-	cic folder: copy to C:\inetpub\wwwroot\<Your CRM Folder>. You should now have a cic folder under C:\inetpub\wwwroot\<Your CRM Folder>.
-	That's it! Go to http://<path to crm> and login, you should see the toolbar underneath the top section

A sample Interaction Attendant profile is also included to let you test popups. The attendant profile performs a lookup of the caller based on its phone number (using the Eic_RemoteId attribute), then retrieves the account full name and updates the Eic_RemoteName attribute accordingly. It also sets the SugarCRM_AccountId attribute in order to perform a screen pop using a pre-defined screen pop action.

The screen pop will work for both this integration and for the Interaction Client/Desktop application.

-	If you do not wish to use the Interaction Attendant profile, you have to do the following for screen pops to work:
	-	Make sure you have accounts that match the phone numbers you will use. The default Interaction Attendant profile uses Eic_RemoteId to lookup the phone number.
	-	Create a custom action in Interaction Administrator under System Configuration/Actions called "SugarCRM" and set the following fields:
	-	http://localhost/crm/index.php?module=Accounts&offset=1&return_module=Accounts&action=DetailView&record=
	-	Make sure your cursor is at the end of URL you just entered and click on "Insert a URL parameter into the URL string..."
		-	Name: AccountId
		-	FriendlyName: AccountId
		-	Override: Allow Override
		-	Default Value: $(SugarCRM_AccountId)
	-	After clicking on OK, your URL should now look like this: http://localhost/crm/index.php?module=Accounts&offset=1&return_module=Accounts&action=DetailView&record={AccountId\}
	-	In Interaction Attendant, use the following actions to do a lookup and set the popup:
	-	Insert a new "Remote Data Query" to login to SugarCRM
		-	URL: http://localhost/<Your CRM Folder>/service/v4_1/soap.php
		-	SOAP Action: http://localhost/<Your CRM Folder>/service/v4_1/soap.php/login
		-	SOAP Input XML:
			`
			<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sug="http://www.sugarcrm.com/sugarcrm" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
			<soapenv:Header/>
			<soapenv:Body>
			<sug:login soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
			  <user_auth xsi:type="sug:user_auth">
			    <user_name xsi:type="xsd:string">vagrant</user_name>
			    <password xsi:type="xsd:string">63623900c8bbf21c706c45dcb7a2c083</password>
			  </user_auth>
			  <application_name xsi:type="xsd:string">CIC</application_name>
			  <name_value_list xsi:type="sug:name_value_list" soapenc:arrayType="sug:name_value[]"/>
			</sug:login>
			</soapenv:Body>
			</soapenv:Envelope>
			`
		-	Data Query Output:
		-	XML Name: id
		-	Attribute Name: SugarCRM_SessionId
	-	Insert a new "Remote Data Query" to get the caller's Contact Id. The following uses Eic_RemoteId to lookup the caller's account.
		-	URL: http://localhost/<Your CRM Folder>/service/v4_1/soap.php
		-	SOAP Action: http://localhost/<Your CRM Folder>/service/v4_1/soap.php/search_by_module
		-	SOAP Input XML:
			`
			<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sug="http://www.sugarcrm.com/sugarcrm" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
			<soapenv:Header/>
			<soapenv:Body>
			<sug:search_by_module soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
			  <session xsi:type="xsd:string">$(SugarCRM_SessionId)</session>
			  <search_string xsi:type="xsd:string">$(Eic_RemoteId)</search_string>
			    <modules xsi:type="sug:select_fields" soapenc:arrayType="xsd:string[]">
			      <item xsi:type="xsd:string">Accounts</item>
			    </modules>
			    <offset xsi:type="xsd:int">0</offset>
			    <max_results xsi:type="xsd:int">1</max_results>
			    <assigned_user_id xsi:type="xsd:string"></assigned_user_id>
			    <select_fields xsi:type="sug:select_fields" soapenc:arrayType="xsd:string[]">
			    <item xsi:type="xsd:string">id</item>
			  </select_fields>
			</sug:search_by_module>
			</soapenv:Body>
			</soapenv:Envelope>
			`
		-	Data Query Output:
		-	XML Name: value
		-	Attribute Name: SugarCRM_AccountId
	-	Insert a new "Remote Data Query" to get the caller's Full Name. This will set Eic_RemoteName (attribute used to display the caller's name in the client) to the account name
		-	URL: http://localhost/<Your CRM Folder>/service/v4_1/soap.php
		-	SOAP Action: http://localhost/<Your CRM Folder>/service/v4_1/soap.php/search_by_module
		-	SOAP Input XML:
			`
			<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sug="http://www.sugarcrm.com/sugarcrm" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
			<soapenv:Header/>
			<soapenv:Body>
			<sug:search_by_module soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
			  <session xsi:type="xsd:string">$(SugarCRM_SessionId)</session>
			  <search_string xsi:type="xsd:string">$(Eic_RemoteId)</search_string>
			  <modules xsi:type="sug:select_fields" soapenc:arrayType="xsd:string[]">
			    <item xsi:type="xsd:string">Accounts</item>
			  </modules>
			  <offset xsi:type="xsd:int">0</offset>
			  <max_results xsi:type="xsd:int">1</max_results>
			  <assigned_user_id xsi:type="xsd:string"></assigned_user_id>
			  <select_fields xsi:type="sug:select_fields" soapenc:arrayType="xsd:string[]">
			    <item xsi:type="xsd:string">name</item>
			  </select_fields>
			</sug:search_by_module>
			</soapenv:Body>
			</soapenv:Envelope>
			`
		-	Data Query Output:
		-	XML Name: value
		-	Attribute Name: Eic_RemoteName
	-	Insert a new "ScreenPop" action with the following properties
		-	Screen Pop Action: SugarCRM (or whatever name you specified when you created the custom action above)
		-	In the "Values" list, edit AccountId and set it to "$(SugarCRM_AccountId)" (without the quotes)

### Known issues
- All statuses are shown as it is not possible via ICWS to get the list of non-selectable statuses without admin priviledges.
- Chrome can sometimes cache data which results in previous versions of the toolbar being loaded. Make sure you clear the cache if this happens.
- Missing record/pause functionalities. Work is in progress.

### Contribution guidelines

-	Laurent Millan
-	Pierrick Lozach