var App = {
  userInteractions: [],
  userStatuses: [],
  currentInteraction: null,
}

// ==================
// -= Queue Events =-
// ==================
var onQueueEvent = function(ev){
  console.group('onQueueEvent');
  //console.log('onQueueEvent: ', ev);

  switch(ev.__type){
    case 'urn:inin.com:queues:queueContentsMessage':
      // Find this interaction
      var interaction;

      // Interaciton Added
      if(ev.interactionsAdded && ev.interactionsAdded.length>0){
        interaction = ICWS.inflateInteraction(ev.interactionsAdded[0]);
        if(interaction.attributes.Eic_ObjectType &&
            interaction.attributes.Eic_ObjectType == "Chat" &&
            interaction.__type == "interaction"){
          interaction = ICWS.inflateChatInteraction(interaction);
        }
        App.userInteractions.push(interaction);
      }

      // Interaction Changed
      if(ev.interactionsChanged && ev.interactionsChanged.length>0){
        console.log('interactionChanged:',ev.interactionsChanged[0].attributes);
        var eventInteraction = ICWS.inflateInteraction(ev.interactionsChanged[0]);
        interaction = _.findWhere(App.userInteractions,{'interactionId': eventInteraction.interactionId});

        var hasRealChanges = false;

        if(!interaction) {
          // Interaction has changed but doesn't exist locally : select this one
          interaction = ICWS.inflateInteraction(eventInteraction);
          if(interaction.attributes.Eic_ObjectType &&
              interaction.attributes.Eic_ObjectType == "Chat" &&
              interaction.__type == "interaction"){
            interaction = ICWS.inflateChatInteraction(interaction);
          }
          App.userInteractions.push(interaction);
          //App.currentInteraction = interaction;
          hasRealChanges = true;
        } else {
          for(var attr=0; attr<Object.keys(eventInteraction.attributes).length; attr++) {
            // Update all attributes
            var attrKey = Object.keys(eventInteraction.attributes)[attr];
            var attrVal = eventInteraction.attributes[attrKey];
            if(interaction.attributes[attrKey] != attrVal ||
                !interaction.attributes.hasOwnProperty(attrKey))
              hasRealChanges = true;

            interaction.attributes[attrKey] = attrVal;
          }
        }
        if (
          Object.keys(eventInteraction.attributes).length == 3 &&
          eventInteraction.attributes.hasOwnProperty('SugarCRM_HasPoppedUp') &&
          eventInteraction.attributes.hasOwnProperty('Eic_Capabilities') &&
          eventInteraction.attributes.hasOwnProperty('Eic_ObjectType')
        ) {
          console.log('Ignoring SugarCRM_HasPoppedUp attribute change');
          return;
        }
        updateActionBar();
      }

      // Interaction Removed
      if(ev.interactionsRemoved && ev.interactionsRemoved.length>0){
        var interactionId = ev.interactionsRemoved[0];
        interaction = _.findWhere(App.userInteractions,{'interactionId': interactionId});
        if (interaction) {
          App.userInteractions = _.reject(App.userInteractions, function(tmpInteraction){
            return interaction.interactionId == tmpInteraction.interactionId});

          interactionDisconnected(interaction);
          return;
        }
      }

      if(interaction && interaction.attributes && interaction.attributes.hasOwnProperty('Eic_State')) {

        // Case Alerting
        if(interaction.attributes.Eic_CallDirection && interaction.attributes.Eic_CallDirection == "I" &&
              interaction.attributes.Eic_State && interaction.attributes.Eic_State == "A"){
          interactionAlerting(interaction);
        }

        // Case Connected
        if(interaction.attributes.Eic_CallDirection && interaction.attributes.Eic_CallDirection == "I" &&
              interaction.attributes.Eic_State && interaction.attributes.Eic_State == "C"){
          console.log("hasRealChanges? " + hasRealChanges);
          if(hasRealChanges)
            interactionConnected(interaction);
        }

        // Case Disconnected
        if(interaction.attributes.Eic_CallDirection && interaction.attributes.Eic_CallDirection == "I" &&
              interaction.attributes.Eic_State &&
              (interaction.attributes.Eic_State == "I" || interaction.attributes.Eic_State == "E")){
          App.userInteractions = _.reject(App.userInteractions, function(tmpInteraction){
            return interaction.interactionId == tmpInteraction.interactionId});

          interactionDisconnected(interaction);
        }
      } else {
        console.log('Interaction state has not changed');
      }
      break;
  }
  console.groupEnd();
}

var getVidyoIntegrationServer = function(callback){
  // Get the VidyoIntegration Server from Server Parameters.
  ICWS.fetch({
    url: '/configuration/server-parameters/SugarCRM_AgentToolbar_VidyoIntegrationServer',
    icwsOptions: { query: [{
        select: "value",
        rightsFilter: "view"
      }]
    },
    success: function(data) {
      console.log("getVidyoIntegrationServer:", data);
      callback(data.value);
    },
    error: function(){

    }
  });
}

var isVidyoInteraction = function(interaction){
  if(interaction.attributes.Eic_ObjectType == "EIC Generic Object" &&
    interaction.attributes.RelatedChat_InteractionId &&
    interaction.attributes.RelatedChat_InteractionId != ""){
    return true;
  }
}

var interactionAlerting = function(interaction){
  console.group("interactionAlerting: ", interaction);

  // If Vidyo interaction related to an existing interaction: its' an escalation
  if(isVidyoInteraction(interaction)){
    /* Interaction is Generic Object +
          specific attribute mention the interaction Id of an existing chat
    */
    for(var i=0; i<App.userInteractions.length; i++){
      var tpmInteraction = App.userInteractions[i];
      if(tpmInteraction.interactionId ==
          interaction.attributes.RelatedChat_InteractionId){
        console.log("Vidyo escalation interaction", interaction);

        // Pickup the Vidyo interaction (Generic Object)
        interaction.pickup();

        // Read the chat URL from attribute
        interaction.getAttributes(['Video_RoomUrl'], function(data){
          // Send the url via a message to customer
          var urlForGuest = data.Video_RoomUrl + "&guestName=" + encodeURI(interaction.attributes.Eic_RemoteName);
          tpmInteraction.sendMessage(urlForGuest);

          // Pop the vidyo chat room for the agent
          console.log("RoomURL:", data.Video_RoomUrl);
          window.open(data.Video_RoomUrl);
        });
        break;
      }
    }
  }
  else {
    interactionSelected(interaction);
  }
  console.groupEnd();
}

var interactionConnected = function(interaction){
  console.log("interactionConnected: ", interaction);
  if(!isVidyoInteraction(interaction)){
    interactionSelected(interaction);
    popup(interaction); //Anything to popup?
  }
}

var goVideo = function(customerName, chatInteractionId){
  // Get the VidyoIntegration Server from Server Parameters.
  getVidyoIntegrationServer(function(integrationService){
    var request = {
      queueName: getConnectionDetails().username,
      queueType: 'User',
      mediaTypeParameters: {
          mediaType: 'GenericInteraction',
          initialState: 'Offering',
          additionalAttributes:[
              {
                  key:'Eic_RemoteName',
                  value:customerName
              },
              {
                  key:'RelatedChat_InteractionId',
                  value:chatInteractionId
              }
          ]
      },
      record: 'true'
    };

    console.log('Request:', request);
    console.log('Server Root:', integrationService);

    // Send request
    $.ajax({
        url: integrationService + '/ininvid/v1/conversations',
        type: 'post',
        data: JSON.stringify(request),
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Request-Headers': 'x-requested-with'
        }
      });
  });

}

var handleChatInteraction = function(interaction, forceReloadHistory){
  console.group("handleChatInteraction", interaction.interactionId);

  // Is getting the history of the chat needed
  var chatHistoryToBeLoaded = (interaction.chatHistory.length == 0);

  var _chatLineAgentTemplate = _.template($('#cic-template-chatline-agent').html());
  var _chatLineCustomerTemplate = _.template($('#cic-template-chatline-customer').html());

  // Inflate chat window interaction
  var $chatWindow = $("#cic_chat_window_"+interaction.interactionId);
  if($chatWindow.length == 0){
    var _chatWindowTemplate = _.template($('#cic-template-chatwindow').html());
    $chatWindow = $(_chatWindowTemplate({
      interactionId: interaction.interactionId,
      title: interaction.attributes.Eic_RemoteName
    }));
    var $chatWindows = $("#cic_chat_windows");
    $chatWindows.append($chatWindow);

    $chatWindow.find('#cic_chatwindow_title').html("Chat with " + interaction.attributes.Eic_RemoteName);

    // Textarea containing the chat message to send
    var $messageToSend = $chatWindow.find('#cic_chatwindow_messagetosend');
    $messageToSend.keyup(function(event) {
      if (event.keyCode == 13 && !event.shiftKey) {
        //console.log("messageToSend>Send Message",$messageToSend.val());
        var text = $messageToSend.val();
        interaction.sendMessage(text.substring(0, text.length - 1));
        $messageToSend.val("");
        return;
      }
    });

    // Match send button to trigger send message
    $chatWindow.find("#cic_chatwindow_send").click(function(){
      interaction.sendMessage($messageToSend.val());
      $messageToSend.val("");
    });

    // Go Video button (vidyo escalation)
    var $goVideoButton = $chatWindow.find("#cic_chatwindow_govideo");
    $goVideoButton.hover(
      function() {
        $( this ).animate({ width: 50, height: 50, bottom: -17, left: -17 }, 200);
      }, function() {
        $( this ).animate({ width: 14, height: 14, bottom: 0, left: 0 }, 200);
      }
    );
    $goVideoButton.click(function(){
      goVideo(interaction.attributes.Eic_RemoteName, interaction.interactionId);
    });

    // Minimize button for the chat
    var $minimizeButton = $chatWindow.find("#cic_chatwindow_minimize");
    $minimizeButton.click(function(){
      if($chatWindow.css('bottom') == "0px"){
        $chatWindow.animate({ bottom: -300 }, 300, function(){
          $minimizeButton.attr('src', 'cic/img/bullet_arrow_up.png');
        });
      }
      else{
        $chatWindow.animate({ bottom: 0 }, 300, function(){
          $minimizeButton.attr('src', 'cic/img/bullet_arrow_down.png');
        });
      }
    });

    // Listen new message record
    interaction.on('newMessage', function(event, newMessage){
      if(interaction.interactionId == newMessage.chatMember.interactionId){
        // Get the time of the message
        var lastUpdate = new Date(
          newMessage.timestamp.replace(/(\d{4})(\d{2})(\d{2})T(\d{2})(\d{2})(\d{2})(\.\d{3}Z)/,
          '$1-$2-$3T$4:$5:$6'));
        lastUpdateString = 'Last Update @ ' +
            ((lastUpdate.getHours()<9)?'0':'') + lastUpdate.getHours() + ':' +
            ((lastUpdate.getMinutes()<9)?'0':'') + lastUpdate.getMinutes() + ':' +
            ((lastUpdate.getSeconds()<9)?'0':'') + lastUpdate.getSeconds();
        switch(newMessage.chatMember.chatMemberType){
          case 1:
            console.log("newMessage 1");
            break;
          case 2: // Agent's talking
            var htmlAgent = _chatLineAgentTemplate({
              message: newMessage.text,
              participant: newMessage.chatMember.displayName,
              lastUpdate: lastUpdateString
            });
            $chatWindow.find('#cic_chatwindow_messages').append(htmlAgent);
            break;
          case 0: // Customer's talking
          case 3: // Customer's talking
          case 4: // Customer's talking
            var htmlCustomer = _chatLineCustomerTemplate({
              message: newMessage.text,
              participant: newMessage.chatMember.displayName,
              lastUpdate: lastUpdateString
            });
            $chatWindow.find('#cic_chatwindow_messages').append(htmlCustomer);
            // Blink title
            if(!newMessage.fromHistory){
              $chatWindow.find('#cic_chatwindow_title').parent().animate({ backgroundColor: "#F60"}, 1500, function(){
                $chatWindow.find('#cic_chatwindow_title').parent().animate({ backgroundColor: "transparent"}, 1500);
              });

              setTimeout(function(){
                var $interaction = $('#cic_toolbar_interaction_' + interaction.interactionId)
                var initialColor = $interaction.css('background-color');
                $interaction.animate({ backgroundColor: "#FF6600"}, 1500, function(){
                $interaction.animate({ backgroundColor: initialColor}, 1500);
              })}, 100);

            }
            break;
        }
        // height of the chat messages ul
        var height = 0;
        $chatWindow.find('#cic_chatwindow_messages').find('li').each(function (id, item) {
          height += $(item).height();
        });
        // scroll down to the bottom of the chat messages
        if(newMessage.fromHistory)
          $chatWindow.find('#cic_chatwindow_messages').scrollTop(height);
        else
          $chatWindow.find('#cic_chatwindow_messages').animate({ scrollTop: height }, 500);

        newMessage.fromHistory = true;
      }
    });

    // Listen add message record
    interaction.on('addMessage', function(event, newMessage){
      if(interaction.interactionId == newMessage.chatMember.interactionId){
        // Get the time of the message
        var lastUpdate = new Date(
          newMessage.timestamp.replace(/(\d{4})(\d{2})(\d{2})T(\d{2})(\d{2})(\d{2})(\.\d{3}Z)/,
          '$1-$2-$3T$4:$5:$6'));
        lastUpdateString = 'Last Update @ ' +
            ((lastUpdate.getHours()<9)?'0':'') + lastUpdate.getHours() + ':' +
            ((lastUpdate.getMinutes()<9)?'0':'') + lastUpdate.getMinutes() + ':' +
            ((lastUpdate.getSeconds()<9)?'0':'') + lastUpdate.getSeconds();
        switch(newMessage.chatMember.chatMemberType){
          case 1:
            console.log("addMessage type 1");
            break;
          case 0: // Customer's talking
          case 3: // Customer's talking
          case 4: // Customer's talking
            // Blink title
            if(!newMessage.fromHistory){
              $chatWindow.find('#cic_chatwindow_title').parent().animate({ backgroundColor: "#F60"}, 1500, function(){
                $chatWindow.find('#cic_chatwindow_title').parent().animate({ backgroundColor: "transparent"}, 1500);
              });

              setTimeout(function(){
                var $interaction = $('#cic_toolbar_interaction_' + interaction.interactionId)
                var initialColor = $interaction.css('background-color');
                $interaction.animate({ backgroundColor: "#FF6600"}, 1500, function(){
                $interaction.animate({ backgroundColor: initialColor}, 1500);
              })}, 100);

            }
          case 2: // Agent's talking
            var html = "</br>\r\n" + newMessage.text;
            $chatWindow.find('#cic_chatwindow_messages li').last().find('p').append(html);
            $chatWindow.find('#cic_chatwindow_messages li').last().find('em').html(lastUpdateString);
            break;
        }

        // height of the chat messages ul
        var height = 0;
        $chatWindow.find('#cic_chatwindow_messages').find('li').each(function (id, item) {
          height += $(item).height();
        });
        // scroll down to the bottom of the chat messages
        if(newMessage.fromHistory)
          $chatWindow.find('#cic_chatwindow_messages').scrollTop(height);
        else
          $chatWindow.find('#cic_chatwindow_messages').animate({ scrollTop: height }, 500);

        newMessage.fromHistory = true;
      }
    });

    // Popup chat window
    $chatWindow.css('visibility', 'visible');

    // If no history : get history from call attributes
    if(chatHistoryToBeLoaded){
      interaction.getAttributes(['Eic_WS_EPConversationHistory'], function(data){
        var history = '<messages>' + data['Eic_WS_EPConversationHistory'] + '</messages>';
        console.log('History: ', data.Eic_WS_EPConversationHistory);
        console.log('History: ', App.test = $.parseXML(history))
        var $history = $($.parseXML(history));
        var chatEvents = {
          __type: 'urn:inin.com:interactions.chat:chatContentsMessage',
          isDelta: true,
          messagesAdded: []
        };
        $history.find('message').each(function(index, message){
          var $message = $(message);
          var chatEvent = {
            chatMember: {
              displayName: $message.find('displayname').html(),
              interactionId: interaction.interactionId,
              userId: $message.find('displayname').html(),
            },
            text: $message.find('content').html(),
            timestamp: $message.find('time').html().replace(
              /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})\.(\d{3})(.*)/,
              "$1$2$3T$4$5$6.$7Z"),
            fromHistory: true
          }
          switch($message.find('partytype').html()){
            case "1": // Customer
              chatEvent.chatMember.chatMemberType = 2; // Customer
              break;
            case "2": // Agent or Customer
              chatEvent.chatMember.chatMemberType = 4; // Agent
              /*if(getConnectionDetails().userDisplayName == chatEvent.chatMember.userId)
                chatEvent.chatMember.chatMemberType = 2; // Agent
              else
                chatEvent.chatMember.chatMemberType = 4; // Customer
                */
              break;
            case "3": // IC Server
              chatEvent.chatMember.chatMemberType = 1; // System
              break;
            default:
              chatEvent.chatMember.chatMemberType = 0; // Unknown
              break;
          }
          chatEvents.messagesAdded.push(chatEvent);
          console.log('Chat History Event:', chatEvent);
        });
        console.log('Chat History Events:', chatEvents);

        interaction.manageChatEvents(chatEvents);
      });
    }

    if(forceReloadHistory){
      for(var i=0; i<interaction.chatHistory.length; i++){
        interaction.chatHistory[i].fromHistory = true;
      }
      var fakeEvents = {
        __type: 'urn:inin.com:interactions.chat:chatContentsMessage',
        isDelta: true,
        messagesAdded: interaction.chatHistory
      }
      interaction.chatHistory = [];
      interaction.manageChatEvents(fakeEvents);
    }

    // Bind - Starts receving ICWS events for this interaction
    interaction.bind();
  }

  console.groupEnd();
}

var interactionDisconnected = function(interaction){
  console.log("interactionDisconnected: ", interaction);
  if(!isVidyoInteraction(interaction)){
    if(App.currentInteraction && (App.currentInteraction.interactionId == interaction.interactionId)){
      App.currentInteraction = null;
    }
    if(interaction.__type == "chatInteraction"){
      $('#cic_chat_window_'+interaction.interactionId).remove();

      // If this interaction is related to a Vidyo Interaction, disconnect it too.
      for(var i=0; i<App.userInteractions.length; i++){
        var tmpInteraction = App.userInteractions[i];
        if(interaction.interactionId == tmpInteraction.attributes.RelatedChat_InteractionId){
          tmpInteraction.disconnect();
          break;
        }
      }
      // Stops listening to ICWS events for this interaction
      interaction.unbind();
    }
    updateActionBar();
  }
}

var interactionSelected = function(interaction){
  if(App.currentInteraction == interaction) return;

  if(isVidyoInteraction(interaction)) return;

  App.currentInteraction = interaction;
  updateActionBar();

  if(App.currentInteraction.__type == "chatInteraction")
    handleChatInteraction(interaction);
}

var popup = function(interaction, force){
  if(force){
    if (interaction.attributes.Eic_ScreenPopName && interaction.attributes.Eic_ScreenPopName == "URL" && interaction.attributes.Eic_ScreenPopData) {
      // We have data to do a screen pop
      var parsedData = parseScreenPopData(interaction.attributes.Eic_ScreenPopData);
      if (parsedData){
        console.log('Browse to ' + parsedData.URL);
        window.location = parsedData.URL;
      }
    }
  }
  else{
    if (interaction.hasPoppedUp()) {
      console.warn('interaction has already popped up');
      return;
    }
    console.log("popup: ", interaction);
    if (interaction.attributes.Eic_ScreenPopName && interaction.attributes.Eic_ScreenPopName == "URL" && interaction.attributes.Eic_ScreenPopData) {
      // We have data to do a screen pop
      var parsedData = parseScreenPopData(interaction.attributes.Eic_ScreenPopData);
      if (parsedData){
        console.log('Browse to ' + parsedData.URL);
        window.location = parsedData.URL;
        setAttribute(interaction, 'SugarCRM_HasPoppedUp', 'true');
      }
    }
  }

}

var parseScreenPopData = function(screenPopData){
  //Sample: POPTYPE=NEW|AccountId=<account id>|URL=http://localhost/crm/index.php?module=Accounts&offset=1&return_module=Accounts&action=DetailView&record={AccountId}
  var result = {};
  $.each(screenPopData.split(/\|/), function (i, val) {
    var arr = val.split(/(.*?)=\s*(.*)/);
    arr[2] && (result[arr[1]] = arr[2]);
  })
  // If AccountId not set > change the URL to a URL to create a new account.
  if(!result.AccountId){
    result.URL = result.URL.replace(/DetailView.*/, "EditView");
  }
  else {
    //Replace {attribute name} with its value
    result.URL = result.URL.replace('{AccountId}', result.AccountId);
  }
  result.URL = result.URL.replace(/\\=/g,'='); //CIC escapes '=' characters. Need to remove \'s
  console.log(result);
  return result;
}

var setAttribute = function(interaction, attributeName, attributeValue) {
  interaction.setAttribute({attributeName: attributeName, attributeValue: attributeValue});
}

// =================
// -= User Status =-
// =================
var onStatusChanged = function(ev){
  console.log('onStatusChanged: ', ev);
  var newStatus = ev.userStatusList[0];
  switch(ev.__type){
    case 'urn:inin.com:status:userStatusMessage':
      updateDisplayedStatus(newStatus.statusId);
      break;
  }
}

var updateDisplayedStatus = function(statusId){
  console.log('updateStatus: ', statusId);
  var status = _.findWhere(App.userStatuses, {'statusId': statusId});
  var i = App.userStatuses.indexOf(status);
  // Select the new status
  $('#cic_toolbar_selectstatus').ddslick('select', {index: String(i) });
  setConnectionDetails({currentStatus: status });
}

/*** Display layout when agent is not logged on: show the login form ***/
var displayAgentNotConnected = function(){
  localStorage.removeItem("cic_agent_toolbar.sessionId");
  localStorage.removeItem("cic_agent_toolbar.csrfToken");

  // Show stats bar
  $("#cic_toolbar_stats").css('visibility', 'hidden');

  // Display logon form
  var _template = _.template($('#cic-template-logonform').html());
  var html = _template({});
  $("#cic_agent_toolbar").html(html);

  // If button clicked > ICWS.Start({...})
  $('#cic_logonform_logon').click(function(){
    var $username = $('#cic_logonform_username');
    var $password = $('#cic_logonform_password');
    var $icServer = $('#cic_logonform_cicserver');
    var $stationType = $('#cic_stationform_type');
    var $station = $('#cic_stationform_name');

    logon($username.val(), $password.val(), $icServer.val(), $stationType.val(), $station.val());
  })
}

// ========================
// -= Action Bar Refresh =-
// ========================
var updateActionBar = function(){
  var capabilities;
  if(!App.currentInteraction)
    capabilities = ICWS.parseCapabilities(0);
  else
    capabilities = App.currentInteraction.parseCapabilities();

  var _template = _.template($('#cic-template-actionbar').html());
  var html = _template({
    interaction: App.currentInteraction,
    capabilities: capabilities
  });
  $("#cic_toolbar_actionbar").html(html);

  // Pickup interaction
  $("#cic_toolbar_Pickup").click(function(){
    App.currentInteraction.pickup();
  });
  // Disconnect interaction
  $("#cic_toolbar_Disconnect").click(function(){
    App.currentInteraction.disconnect();
  });
  // On hold interaction
  $("#cic_toolbar_Hold").click(function(){
    App.currentInteraction.hold();
  });
  // On mute interaction
  $("#cic_toolbar_Mute").click(function(){
    App.currentInteraction.mute();
  });
  // On transfer interaction
  $("#cic_toolbar_Transfer").click(function(){
    console.log('Transferring interaction');
    var options = {
      target: $("#cic_toolbar_Transfer_target").val()
    };
    App.currentInteraction.transfer(options);
  });
  // On record interaction
  $("#cic_toolbar_Record").click(function(){
    App.currentInteraction.record();
  });


  // On transfer target change
  $("#cic_toolbar_Transfer_target").on('input', function() {
    if (App.currentInteraction && App.currentInteraction.capabilities && App.currentInteraction.capabilities.Transfer && $("#cic_toolbar_Transfer_target").val()) {
      $("#cic_toolbar_Transfer").prop('disabled', false);
    }
  });

  // Update interaction list
  var _interactionTemplate = _.template($('#cic-template-interaction').html());
  $('#cic_toolbar_interactions').html("");
  _.each(App.userInteractions, function(interaction){
    // Prevent showing Recorder Object interactions
    if(interaction.attributes.Eic_ObjectType &&
        interaction.attributes.Eic_ObjectType != "Recorder Object" &&
        !isVidyoInteraction(interaction)){

        var color = "#337ab7";
        if(interaction.attributes.Eic_State){
          switch(interaction.attributes.Eic_State){
            case "A": color = "#f0ad4e"; break;
            case "C": color = "#5cb85c"; break;
            case "I": color = "#d9534f"; break;
            case "E": color = "#d9534f"; break;
          }
        }
        var selected = App.currentInteraction && (App.currentInteraction.interactionId == interaction.interactionId)

        var interactionHtml = _interactionTemplate({
          interaction: interaction,
          color: color,
          selected: selected
        });
        $('#cic_toolbar_interactions').append(interactionHtml);
        $('#cic_toolbar_interaction_'+interaction.interactionId).click(function(){
          interactionSelected(interaction);
        });
        $('#cic_toolbar_interaction_goto_'+interaction.interactionId).click(function(){
          popup(interaction, true);
        });
    }
  });
}

/*** Display layout when agent is connected: show the toolbar ***/
var displayAgentConnected = function(){
  var cDetails = getConnectionDetails();

  // Draw actionbar base template
  var _template = _.template($('#cic-template-activetoolbar').html());
  var html = _template({});
  $("#cic_agent_toolbar").html(html);

  // Add buttons in action bar
  updateActionBar();

  // Show stats bar
  $("#cic_toolbar_stats").css('visibility', 'visible');

  // Add status items
  var _templateStatus = _.template($('#cic-template-selectstatus').html());
  var htmlStatus = _templateStatus({});
  $("#cic_toolbar_statusbar").html(htmlStatus);

  $("#cic_toolbar_Logoff").click(function(){
    logoff();
  });

  // Get selectable statuses
  ICWS.fetch({
      url: '/status/status-messages',
      icwsOptions: { query: [{
          select: 'configurationId, messageText, iconUri, statusId',
          rightsFilter: 'view'
        }]
      },
      success: function(statuses) {
        console.log('statuses:', statuses);
        App.userStatuses = statuses.statusMessageList;

        // Get the list of status messages that can be accessed by this user
        ICWS.fetch({
          url: '/status/status-messages-user-access/' + cDetails.username,
          icwsOptions: {},
          success: function(allowedStatuses) {
            console.log('Allowed statuses: ', allowedStatuses);
            App.allowedUserStatuses = allowedStatuses.statusMessages;

            var ddSlickData = [];
            for(var i=0; i<statuses.statusMessageList.length; i++){
              var status = statuses.statusMessageList[i];
              if (App.allowedUserStatuses.indexOf(status.statusId) > -1) {
                ddSlickData.push({
                  text: status.messageText,
                  value: status.statusId,
                  selected: false,
                  selectable: status.isSelectableStatus,
                  imageSrc: ICWS.baseURI() + status.iconUri
                });
              }
            }

            // Generate a select option with icons using ddslick.js
            $("#cic_toolbar_selectstatus").ddslick({
              data: ddSlickData,
              width: 200,
              imagePosition: "left",
              selectText: "Select status",
              onSelected: function(selectedStatus){
                console.log('selected:', selectedStatus);
                // Update the status
                ICWS.update({
                    url: '/status/user-statuses/' + cDetails.username,
                    icwsOptions: {},
                    representation: {
                      statusId: selectedStatus.selectedData.value
                    },
                    success: function(data){
                      console.log('status changed: ', data);
                    },
                    error: function(data){
                      console.log('error changing status: ', data);
                    }
                });
              }
            });

            // Get the current status
            ICWS.fetch({
              url: '/status/user-statuses',
              id: cDetails.username,
              icwsOptions: {},
              success: function(currentStatuse){
                updateDisplayedStatus(currentStatuse.statusId);
              },
              error: function(data){
              }
            });

          },
          error: function(data) {
            console.log('Allowed statuses error:', data);
          }
        });
      },
      error: function(data) {
        console.log('error statuses: ', data);
      }
  });
}

// ================
// -= Connection =-
// ================
/*** Set connection details variables in localStorage ***/
var setConnectionDetails = function(options){
  if(options){
    if(options.station){
      localStorage.setItem("cic_agent_toolbar.icserver", options.icserver);
    }
    if(options.station){
      localStorage.setItem("cic_agent_toolbar.station", options.station);
    }
    if(options.stationType){
      localStorage.setItem("cic_agent_toolbar.stationType", options.stationType);
    }
    if(options.currentStatus){
      localStorage.setItem("cic_agent_toolbar.currentStatus", options.currentStatus);
    }
    if(options.interactionCount){
      localStorage.setItem("cic_agent_toolbar.interactionCount", JSON.stringify(options.interactionCount));
    }
    if(options.workgroupStats){
      localStorage.setItem("cic_agent_toolbar.workgroupStats", JSON.stringify(options.workgroupStats));
    }
  }
}

/*** Set connection details variables from localStorage ***/
var getConnectionDetails = function(){
  return {
    icServer: localStorage.getItem("cic_agent_toolbar.icServer"),
    userDisplayName: localStorage.getItem("cic_agent_toolbar.userDisplayName"),
    username: localStorage.getItem("cic_agent_toolbar.username"),
    station: localStorage.getItem("cic_agent_toolbar.station"),
    stationType: localStorage.getItem("cic_agent_toolbar.stationType"),
    currentStatus: localStorage.getItem("cic_agent_toolbar.currentStatus"),
    interactionCount: JSON.parse(localStorage.getItem("cic_agent_toolbar.interactionCount")),
    workgroupStats: JSON.parse(localStorage.getItem("cic_agent_toolbar.workgroupStats")),
  }
}

/*** Clear connection details variables from localStorage ***/
var resetConnectionDetails = function(){
  localStorage.removeItem("cic_agent_toolbar.icServer");
  localStorage.removeItem("cic_agent_toolbar.userDisplayName");
  localStorage.removeItem("cic_agent_toolbar.username");
  localStorage.removeItem("cic_agent_toolbar.station");
  localStorage.removeItem("cic_agent_toolbar.stationType");
  localStorage.removeItem("cic_agent_toolbar.currentStatus");
  localStorage.removeItem("cic_agent_toolbar.interactionCount");
  localStorage.removeItem("cic_agent_toolbar.workgroupStats");
}

/*** logon the agent with parameters ***/
var logon = function(username, password, icServer, stationType, station){
  ICWS.start({
    applicationName: "Basic Agent Toolbar",
    username: username,
    password: password,
    icServerHostname: icServer,
    loginSuccess: function(logonInfo){
      // Login successfull
      console.log("Check logon: ", logonInfo);

      if(logonInfo.errorId){
        displayAgentNotConnected();
      }
      else{
        // Keep track of ICWS session in localStorage to avoid login when refreshing the page
        localStorage.setItem("cic_agent_toolbar.sessionId", ICWS.sessionId);
        localStorage.setItem("cic_agent_toolbar.csrfToken", ICWS.csrfToken);
        localStorage.setItem("cic_agent_toolbar.username", username);
        localStorage.setItem("cic_agent_toolbar.icServer", logonInfo.icServer);
        localStorage.setItem("cic_agent_toolbar.userDisplayName", logonInfo.userDisplayName);

        var connectionDetails = getConnectionDetails();

        // Start pulling messages
        ICWS.MessageManager.start();

        // Get station details
        ICWS.fetch({
            url: '/connection/station',
            icwsOptions: {},
            success: function(data){
              // Managed to get existing station details
              console.log('displayAgentLoggedOn/connectionStation[success]: ', data);
              displayAgentConnected();
            },
            error: function(data){
              // If no station
              console.log('displayAgentLoggedOn/connectionStation[error]: ', data);

              var representation = {};
              switch(stationType){
                case 'workstation':
                  representation = {
                    __type: "urn:inin.com:connection:workstationSettings",
                    supportedMediaTypes: [
                      1, // Call
                      2, // Chat
                      3, // Email
                      4, // Generic Object
                      5, // Callback
                      6, // SMS
                      7  // Work Item (IPA)
                    ],
                    readyForInteractions: true,
                    workstation: station
                  };
                  break;
                case 'remote_workstation':
                  representation = {
                    __type: "urn:inin.com:connection:remoteWorkstationSettings",
                    supportedMediaTypes: [
                      1, // Call
                      2, // Chat
                      3, // Email
                      4, // Generic Object
                      5, // Callback
                      6, // SMS
                      7  // Work Item (IPA)
                    ],
                    readyForInteractions: true,
                    workstation: station
                  };
                  break;
                case 'remote_number':
                  representation = {
                    __type: "urn:inin.com:connection:remoteNumberSettings",
                    supportedMediaTypes: [
                      1, // Call
                      2, // Chat
                      3, // Email
                      4, // Generic Object
                      5, // Callback
                      6, // SMS
                      7  // Work Item (IPA)
                    ],
                    readyForInteractions: true,
                    remoteNumber: station
                  };
                  break;
              }

              // Update the station (connect to the station)
              ICWS.update({
                  url: '/connection/station',
                  icwsOptions: {},
                  representation: representation,
                  success: function(data){
                    console.log('displayAgentLoggedOn/connectionStation[success]: ', data);
                    setConnectionDetails({station: station, stationType: stationType});

                    listenNewEvents();

                    displayAgentConnected();
                  },
                  error: function(data){
                    console.log('displayAgentLoggedOn/connectionStation[error]: ', data);
                    displayAgentNotConnected();
                    ICWS.MessageManager.stop();
                  }
                });
            }
          });
      }
    }
  });
}

/*** Starts stats monitoring for the agent ***/
var startStatsMonitoring = function(){
  // The list of stats to listen to.
  var statsData = [];

  // Interaction Answered
  statsData.push({
    "statisticIdentifier":"inin.workgroup:InteractionsAnswered",
		"parameterValueItems":[
      {
        "parameterTypeId":"ININ.People.WorkgroupStats:Workgroup",
        "value":"Customer Service"
      },
      {
        "parameterTypeId":"ININ.Queue:Interval",
        "value":"CurrentPeriod"
      }]
    });

  // Interactions in queue
  statsData.push({
    "statisticIdentifier":"inin.queue:InteractionCount",
		"parameterValueItems":[
      {
        "parameterTypeId":"ININ.Queue:Type",
        "value":"Workgroup"
      },
      {
        "parameterTypeId":"ININ.Queue:Name",
        "value":"Customer Service"
      }]
    });

  // Agents available
  statsData.push({
    "statisticIdentifier":"inin.workgroup:NumberAvailableForACDInteractions",
		"parameterValueItems":[
      {
        "parameterTypeId":"ININ.People.WorkgroupStats:Workgroup",
        "value":"Customer Service"
      }]
    });
  // Agents not available
  statsData.push({
    "statisticIdentifier":"inin.workgroup:NotAvailable",
		"parameterValueItems":[
      {
        "parameterTypeId":"ININ.People.WorkgroupStats:Workgroup",
        "value":"Customer Service"
      }]
    });
  // Agents logged in and activated
  statsData.push({
    "statisticIdentifier":"inin.workgroup:AgentsLoggedInAndActivated",
		"parameterValueItems":[
      {
        "parameterTypeId":"ININ.People.WorkgroupStats:Workgroup",
        "value":"Customer Service"
      }]
    });
  // Service level target
  statsData.push({
    "statisticIdentifier":"inin.workgroup:ServiceLevelTarget",
		"parameterValueItems":[
      {
        "parameterTypeId":"ININ.People.WorkgroupStats:Workgroup",
        "value":"Customer Service"
      },
      {
        "parameterTypeId":"ININ.Queues:InteractionType",
        "value":"Chat"
      },
      {
        "parameterTypeId":"ININ.Queue:Interval",
        "value":"CurrentPeriod"
      }]
    });

    // Start the monitoring
    monitorStats(statsData);
}

/*** Graphically update the interaction in queue count ***/
var updateInteractionCount = function(interactionCount){
  var sparklineOptions = {
    type: 'line',
    width: '48px',
    height: '24px',
    normalRangeMin: 0,
  };

  // Get the interaction in queue count history from localStorage
  var count = getConnectionDetails().interactionCount;
  if(!count) count = [];
  // Add the new interactions in queue count
  count.push(interactionCount);
  // Remove the oldest count
  if(count.length > 50)
    count.shift();

  // Keep the new interaction count in the localStorage
  setConnectionDetails({interactionCount: count});
  // Update the sparkline
  $("#cic_toolbar_interactioncount").sparkline(count, sparklineOptions);
  $("#cic_toolbar_interactionnumber").html(interactionCount);

  // Reset an automatic interval
  if(App.interactionCountInterval) clearInterval(App.interactionCountInterval);
  // Start the interval to make the sparkline dynamic (if no update of the stat : add the last known value every second)
  App.interactionCountInterval = setInterval(function(){
    // If nothing has changed, I will add the last known number of interactions in queue
    // Add the new interactions in queue count
    count.push(interactionCount);
    // Remove the oldest count
    if(count.length > 50)
      count.shift();

      // Keep the new interaction count in the localStorage
    setConnectionDetails({interactionCount: count});
    // Update the sparkline
    $("#cic_toolbar_interactioncount").sparkline(count, sparklineOptions);
    $("#cic_toolbar_interactionnumber").html(interactionCount);
  }, 1000);

}

var updateWorkgroupStats = function(type, value){
  var sparklineOptions = {
    type: 'pie',
    width: '24px',
    height: '24px',
    sliceColors: ['#d9534f','#5cb85c']//, '#5bc0de']
  };
  var workgroupStats = getConnectionDetails().workgroupStats;
  if(!workgroupStats) workgroupStats = {
    AgentsLoggedInAndActivated: 0,
    NotAvailable: 0,
    NumberAvailableForACDInteractions: 0
  };

  workgroupStats[type] = value;

  var count = [workgroupStats.NotAvailable,
    workgroupStats.NumberAvailableForACDInteractions];
  $("#cic_toolbar_workgroupstats").sparkline(count, sparklineOptions);

  setConnectionDetails({workgroupStats: workgroupStats});
}

var updateServiceLevelTargetStats = function(type, value){
  $("#cic_toolbar_serviceLevel").html(value);
  console.log("updateServiceLevelTargetStats", value);
}

var onStatChanged = function(data){
  for(var i=0;i<data.statisticValueChanges.length;i++){
    var changedStat = data.statisticValueChanges[i];
    if(changedStat.statisticValue){
      switch(changedStat.statisticKey.statisticIdentifier){
        case "inin.queue:InteractionCount":
          updateInteractionCount(changedStat.statisticValue.value);
          break;
        case "inin.workgroup:AgentsLoggedInAndActivated":
          updateWorkgroupStats("AgentsLoggedInAndActivated", changedStat.statisticValue.value);
          break;
        case "inin.workgroup:NotAvailable":
          updateWorkgroupStats("NotAvailable", changedStat.statisticValue.value);
          break;
        case "inin.workgroup:NumberAvailableForACDInteractions":
          updateWorkgroupStats("NumberAvailableForACDInteractions", changedStat.statisticValue.value);
          break;
        case "inin.workgroup:ServiceLevelTarget":
          if(changedStat.statisticValue.value)
            updateServiceLevelTargetStats("ServiceLeveltarget", changedStat.statisticValue.value);
          break;
      }
    }
  }
  console.log("onStatChanged", data);
}

var monitorStats = function(statisticValueSubscription){
  ICWS.MessageManager.on({
    messageType: 'urn:inin.com:statistics:statisticValueMessage',
    url: '/messaging/subscriptions/statistics/statistic-values',
    icwsOptions: {},
    representation: {
      statisticKeys: statisticValueSubscription
    },
    handleEvent: onStatChanged
  });

  updateInteractionCount(0);
}

var listenNewEvents = function(){
  var connectionDetails = getConnectionDetails();

  // Listen to queue messages
  ICWS.MessageManager.on({
      messageType: 'urn:inin.com:queues:queueContentsMessage',
      url: '/messaging/subscriptions/queues/userQueue',
      icwsOptions: {},
      representation: {
        queueIds:[
          { queueType: 1, queueName: connectionDetails.username }
        ],
        attributeNames: ['Eic_CallDirection', 'Eic_State', 'Eic_RemoteAddress',
          'Eic_Capabilities', 'Eic_ObjectType', 'Eic_ScreenPopName',
          'Eic_ScreenPopData', 'SugarCRM_AccountId', 'SugarCRM_HasPoppedUp',
          'Eic_Muted', 'Eic_Recorders', 'Eic_RemoteName',
          'RelatedChat_InteractionId', 'Eic_AssignedWorkgroup']
      },
      handleEvent: onQueueEvent
  });

  // Listen to status messages
  ICWS.MessageManager.on({
      messageType: 'urn:inin.com:status:userStatusMessage',
      url: '/messaging/subscriptions/status/user-statuses',
      icwsOptions: {},
      representation: {
        userIds:[connectionDetails.username],
      },
      handleEvent: onStatusChanged
  });

  // Listen to stats updateStatus
  startStatsMonitoring();
}

var logoff = function(){
  ICWS.delete({
    url: '/connection',
    icwsOptions: {},
    success: function(data){
      console.log('disconnect[success]: ', data);
      resetConnectionDetails();
      ICWS.MessageManager.stop();
      displayAgentNotConnected();
    },
    error: function(data){
      console.log('displayAgentLoggedOn/connectionStation[error]: ', data);
      displayAgentNotConnected();
    }
  });
}

// ===========
// -= Start =-
// ===========
/*** Start the web app : if agent logged on show the toolbar ***/
var start = function(){
  function supports_html5_storage() {
    try {
      return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
      return false;
    }
  }

  // Check if localStorage is initialized
  if(localStorage){
    var icServer = localStorage.getItem("cic_agent_toolbar.icServer");
    var sessionId = localStorage.getItem("cic_agent_toolbar.sessionId");
    var csrfToken = localStorage.getItem("cic_agent_toolbar.csrfToken");
    ICWS.sessionId = sessionId;
    ICWS.csrfToken = csrfToken;

    if(sessionId && csrfToken){
      ICWS.URI_SERVER = icServer;
      // Check if existing connection is open
      ICWS.fetch({
          url: ICWS.URL.Connection,
          icwsOptions: {},
          success: function(data){
            console.log("Check existing connection: ", data);

            if (data.errorCode) {
              // An error occurred during the login
              displayAgentNotConnected();
            } else {
              // Session exists or existed
              switch(data.connectionState){
                case 0: // None
                  displayAgentNotConnected();
                  break;
                case 1: // Up
                  // Start pulling messages
                  ICWS.MessageManager.start();
                  // Listen to new events
                  listenNewEvents();
                  displayAgentConnected();
                  break;
                case 2: // Down
                  displayAgentNotConnected();
                  break;
              }
            }
          },
          error: function(data){
            console.log("Check existing connection: ", data);

            // Invalid parameters : not logged
            displayAgentNotConnected();
          }
        });
    }
    else{
      // No info in localStorage : not connected
      displayAgentNotConnected();
    }
  }
}

start();
